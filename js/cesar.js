window.addEventListener('load', function() {
    const encryptRadio = document.querySelector('#encrypt');
    const decryptRadio = document.querySelector('#decrypt');

    const decryptInput = document.querySelector('#decrypted');
    const encryptInput = document.querySelector('#encrypted');

    const shift = document.querySelector('#shift');
    const submit = document.querySelector('#submit');

    encryptRadio.addEventListener('change', () => {
        decryptInput.removeAttribute('disabled');
        encryptInput.setAttribute('disabled', true);

        encryptInput.value = '';
        submit.innerHTML = 'szyfruj';
    });

    decryptRadio.addEventListener('change', () => {
        decryptInput.setAttribute('disabled', true);
        encryptInput.removeAttribute('disabled');

        decryptInput.value = '';
        submit.innerHTML = 'deszyfruj';
    });

    submit.addEventListener('click', () => {
        let source;
        let result;
        let shiftValue = parseInt(shift.value);

        if (submit.innerHTML === 'szyfruj') {
            source = decryptInput.value;
            result = encryptInput;
        } else {
            source = encryptInput.value;
            result = decryptInput;
            shiftValue = -shiftValue;
        }

        result.value = cesar(source, shiftValue);
    })
})