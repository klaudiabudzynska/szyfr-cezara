let alphabet ="aąbcćdeęfghijklłmnńoópqrsśtuvwxyzźż".split('');

const minifyText = (text) => {
    return text.toLowerCase().split('').filter(letter => {
        return alphabet.indexOf(letter) !== -1;
    }).join('');
}