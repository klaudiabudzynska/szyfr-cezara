const cesar = (text, shift) => {
    let result = '';
    text = minifyText(text);

    for (let i = 0; i < text.length; i++) {
        let indexOfEncrypted = alphabet.indexOf(text[i]) + shift;

        if (indexOfEncrypted > alphabet.length - 1) {
            indexOfEncrypted = indexOfEncrypted - alphabet.length - 1;
        }

        result += alphabet[indexOfEncrypted];
    }
    return result;
}